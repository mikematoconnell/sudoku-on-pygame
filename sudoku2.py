import pygame



pygame.init()
display_width = 900
display_height = 900

font = pygame.font.SysFont(None, 50)

white = (255, 255, 255)
grey = (211,211,211)
blue = (0, 0, 255)
black = (0, 0, 0)


window_display = pygame.display.set_mode([display_width, display_height])
pygame.display.set_caption("Sudoku")


class Button_Start_Menu():
    def __init__(self, x, y, width, height, message) :       # needs a x and y coordinate as well as a width and height
        self.message = message
        # create a text surface object where 
        self.text = font.render(message, True, black, blue)
        # create a rectangular object for the text surface object using get_rect() method of a pygame text surface object
        self.rect = self.text.get_rect()
        # set the posistion of the rectangular object by setting the value of the center property with the x and y values
        self.rect.center = (x + (width/2), y + (height/2))
        self.clicked = False
    def draw(self):
        pos = pygame.mouse.get_pos()
        if self.rect.collidepoint(pos) and self.clicked == False and pygame.mouse.get_pressed()[0] == 1:
            self.clicked = True
        elif self.rect.collidepoint(pos):
            self.text = font.render(self.message, True, black, grey)
        # if user clicks outside of button it unclicks current button
        elif self.rect.collidepoint(pos) == False and self.clicked == True and pygame.mouse.get_pressed()[0] == 1:
            self.clicked = False
        else:
            self.text = font.render(self.message, True, black, blue)
        if self.clicked == True:
            self.text = font.render(self.message, True, black, white)

        window_display.blit(self.text, self.rect)
    def delete(self):
        if len(self.message) != 0:
            self.message = self.message[0:-1]
    def update(self, event):
        self.message += event.unicode 


start = Button_Start_Menu(5, 20, 200, 200, "Start")

# different functionality need two buttons
# class Button_Grid():
#     def __init__(self,):
        # needs and x and y coordinate, along with width and height though these will always be the same 
        # needs a is clicked property that is initially set to False
run = True
while run:
    window_display.fill(white)
    start.draw()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if start.clicked == True:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE:
                    start.delete()
                elif event.key == pygame.K_ESCAPE:
                    run = False
            
                else: 
                    start.update(event)
        pygame.display.update()
pygame.quit()
quit()


# def intro_screen():
#     intro = True
#     while intro == True:
#         start.draw()
#         for event in